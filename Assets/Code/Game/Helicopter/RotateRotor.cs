﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnum;

public class RotateRotor : MonoBehaviour
{
    public RotorAxisRotation rotateAxis;
    public float currentRotationSpeed;
    // Update is called once per frame
    void Update()
    {
        switch (rotateAxis)
        {
            case RotorAxisRotation.X:
                transform.Rotate(currentRotationSpeed * Time.deltaTime, 0, 0);
                break;
            case RotorAxisRotation.Y:
                transform.Rotate(0, currentRotationSpeed * Time.deltaTime, 0);
                break;
            case RotorAxisRotation.Z:
                transform.Rotate(0, 0, currentRotationSpeed * Time.deltaTime);
                break;
        }
    }
}
