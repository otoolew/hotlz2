﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(HelicopterUnit))]
[RequireComponent(typeof(HelicopterMovement))]
public class HelicopterController : MonoBehaviour
{
    [SerializeField] private HelicopterMovement helicopterMovement;
    public HelicopterMovement HelicopterMovement { get => helicopterMovement; set => helicopterMovement = value; }


    [SerializeField] private bool grounded;
    public bool Grounded { get => grounded; set => grounded = value; }
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void FixedUpdate()
    {
        if (grounded)
        {
            helicopterMovement.VerticalFlight();
            return;
        }
        helicopterMovement.HorizontalFlight();
        helicopterMovement.VerticalFlight();
        helicopterMovement.MouseRotation();
    }
    private void OnTriggerEnter(Collider other)
    {
        if ((!other.isTrigger) && (other.gameObject.layer == LayerMask.NameToLayer("Terrain")))
        {
            grounded = true;
        }

        if ((other.isTrigger) && (other.gameObject.layer == LayerMask.NameToLayer("Terrain")))
        {
            grounded = true;
        }      
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.layer == LayerMask.NameToLayer("Terrain"))
        {
            grounded = false;
        }
    }
}
