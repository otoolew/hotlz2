﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelicopterWeaponController : MonoBehaviour
{
    [SerializeField] private WeaponComponent equippedWeapon;
    public WeaponComponent EquippedWeapon { get => equippedWeapon; set => equippedWeapon = value; }

    [SerializeField] private WeaponComponent rocketWeapon;
    public WeaponComponent RocketWeapon { get => rocketWeapon; set => rocketWeapon = value; }

    public LayerMask layerMask;
    private void Start()
    {
        WeaponComponent[] weaponComponents = FindObjectsOfType<WeaponComponent>();

        ////equippedWeapon = weaponComponents[0];
        //for (int i = 0; i < weaponComponents.Length; i++)
        //{
        //    weaponComponents[i].FactionComponent = GetComponent<FactionComponent>();
        //}
    }
    private void Update()
    {
        AimPoint();
        if (Input.GetMouseButton(0))
            equippedWeapon.Fire();
        if (Input.GetMouseButtonDown(1))
            rocketWeapon.Fire();

    }

    private void AimPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit rayHit, layerMask))
        {
            Vector3 hitPoint = rayHit.point;
            Vector3 targetDir = hitPoint - equippedWeapon.transform.position;
            Vector3 newDir = Vector3.RotateTowards(equippedWeapon.transform.forward, targetDir, 10f, 0.0f);
            equippedWeapon.transform.rotation = Quaternion.LookRotation(targetDir);
        }
    }
}
