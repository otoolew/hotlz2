﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierPool : Pool
{
    [SerializeField] private Factory factory;
    public Factory Factory { get => factory; set => factory = value; }

    [SerializeField] private FactionManager factionManager;
    public override FactionManager FactionManager { get => factionManager; set => factionManager = value; }

    [SerializeField] private int poolCount;
    public override int PoolCount { get => poolCount; set => poolCount = value; }

    [SerializeField] private List<GameObject> pooledObjectList;
    public override List<GameObject> PooledObjectList { get => pooledObjectList; protected set => pooledObjectList = value; }

    // Start is called before the first frame update
    void Start()
    {
        FactionManager = GetComponentInParent<FactionManager>();
        FillPool(PoolCount);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public override void CreateObject()
    {
        GameObject soldier = factory.Create(this);
        //soldier.GetComponent<SoldierController>().FactionManager = FactionManager;
        soldier.GetComponent<SoldierNavigation>().CurrentTerritory = FactionManager.HomeTerritory;
        soldier.GetComponent<SoldierNavigation>().CurrentDestinationArea = FactionManager.FactionEntry;
        soldier.transform.SetPositionAndRotation(FactionManager.SoldierSpawnPoint.position, FactionManager.SoldierSpawnPoint.rotation); 

        pooledObjectList.Add(soldier);
    }

    public override void FillPool(int count)
    {
        for (int i = 0; i < count; i++)
        {
            CreateObject();
        }
    }

    public override void ReturnToPool(GameObject go)
    {
        go.transform.parent = transform;
        go.transform.SetPositionAndRotation(FactionManager.SoldierSpawnPoint.position, FactionManager.SoldierSpawnPoint.rotation);
        ResetHealth(go.GetComponent<Damageable>());
        ResetTargetting(go.GetComponent<SoldierController>().TargettingComponent);
        go.GetComponent<SoldierNavigation>().CurrentTerritory = FactionManager.HomeTerritory;
        go.GetComponent<SoldierNavigation>().CurrentDestinationArea = FactionManager.FactionEntry;
        go.GetComponent<IPoolable>().Pooled = true;
        go.SetActive(false);
    }
    public override GameObject FetchFromPool()
    {
        for (int i = 0; i < pooledObjectList.Count; i++)
        {
            if (!pooledObjectList[i].activeSelf)
            {
                return pooledObjectList[i];
            }
        }
        return null;
    }

    private void ResetHealth(Damageable damageable)
    {
        damageable.CurrentHP = damageable.MaxHP;
        damageable.Dead = false;
    }
    private void ResetTargetting(TargettingComponent targettingComponent)
    {
        targettingComponent.ResetTargetter();
    }
}
