﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Pool : MonoBehaviour
{
    public abstract FactionManager FactionManager { get; set; }
    public abstract int PoolCount { get; set; }
    public abstract List<GameObject> PooledObjectList { get; protected set; }
    public abstract void FillPool(int count);
    public abstract void CreateObject();
    public abstract GameObject FetchFromPool();
    public abstract void ReturnToPool(GameObject gameObject);
}
