﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MunitionPool : Pool
{
    [SerializeField] private Factory factory;
    public Factory Factory { get => factory; set => factory = value; }

    [SerializeField] private FactionManager factionManager;
    public override FactionManager FactionManager { get => factionManager; set => factionManager = value; }

    [SerializeField] private int poolCount;
    public override int PoolCount { get => poolCount; set => poolCount = value; }

    [SerializeField] private List<GameObject> pooledObjectList;
    public override List<GameObject> PooledObjectList { get => pooledObjectList; protected set => pooledObjectList = value; }

    // Start is called before the first frame update
    void Start()
    {
        FactionManager = GetComponentInParent<FactionManager>();
        FillPool(PoolCount);
    }

    public override void FillPool(int count)
    {
        for (int i = 0; i < count; i++)
        {
            CreateObject();
        }
    }

    public override void CreateObject()
    {
        pooledObjectList.Add(factory.Create(this));
    }

    public override void ReturnToPool(GameObject gameObject)
    {
        gameObject.transform.parent = transform;
        gameObject.transform.position = Vector3.zero;
        gameObject.GetComponent<IPoolable>().Pooled = true;

        gameObject.GetComponent<Collider>().enabled = false;
        gameObject.GetComponent<Munition>().SelfDestructTimer.ResetTimer();

        gameObject.SetActive(false);
    }

    public override GameObject FetchFromPool()
    {
        for (int i = 0; i < pooledObjectList.Count; i++)
        {
            if (!pooledObjectList[i].activeSelf)
            {
                pooledObjectList[i].GetComponent<Munition>().SelfDestructTimer.ResetTimer();
                return pooledObjectList[i];
            }
        }
        return null;
    }

}
