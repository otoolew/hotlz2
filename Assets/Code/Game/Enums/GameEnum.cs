﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GameEnum
{
    [Serializable] public enum GameState { RUNNING, PAUSED, SCENECHANGE, GAMEOVER }
    [Serializable] public enum RotorAxisRotation { X, Y, Z }
    [Serializable] public enum FactionAlignmentType { NONE, BLUE, RED }
    [Serializable] public enum TargetType { SOLDIER, HELICOPTER, VEHICLE, TOWER, STRUCTURE }
    [Serializable] public enum DestinationType { POINT, GARRISON, HEADQUARTERS, SEEK_ENEMY }
    [Serializable] public enum DestinationSelectMode { FIXED, DISTRIBUTED, RANDOM }
    [Serializable] public enum CheckPointType { START, MIDPOINT, ENDPOINT }
}

