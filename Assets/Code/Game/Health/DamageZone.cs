﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageZone : MonoBehaviour
{
    public HealthComponent healthComponent;

    // Start is called before the first frame update
    void Start()
    {
        if(healthComponent == null)
            healthComponent = transform.root.GetComponent<HealthComponent>();
    }

    public void ApplyDamage(float weaponDamage)
    {
        if(healthComponent != null)
            healthComponent.ApplyDamage(weaponDamage);
    }
}
