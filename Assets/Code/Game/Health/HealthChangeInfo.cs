﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct HealthChangeInfo
{
    public Damageable damageable;

    public float oldHealth;

    public float newHealth;

    public float HealthDifference
    {
        get { return newHealth - oldHealth; }
    }

    public float AbsoluteHealthDifference
    {
        get { return Mathf.Abs(HealthDifference); }
    }
}
