﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvent;
using System;

public class HealthComponent : MonoBehaviour
{

    [SerializeField] private float maxHP;
    public float MaxHP { get => maxHP; set => maxHP = value; }

    [SerializeField] private float currentHP;
    public float CurrentHP { get => currentHP; set => currentHP = value; }

    [SerializeField] private bool dead;
    public bool Dead { get => dead; set => dead = value; }

    public DieEvent OnDie;

    // Start is called before the first frame update
    void Start()
    {
        currentHP = maxHP;
    }
    public float CurrentHealth { protected set; get; }

    /// <summary>
    /// Gets the normalised health.
    /// </summary>
    public float NormalisedHealth
    {
        get
        {
            if (Math.Abs(MaxHP) <= Mathf.Epsilon)
            {
                Debug.LogError("Max Health is 0");
                MaxHP = 1f;
            }
            return CurrentHealth / MaxHP;
        }
    }
    public void ApplyDamage(float amount)
    {
        if (!dead)
        {
            currentHP -= amount;
            if (currentHP <= 0)
            {
                OnDie.Invoke();
                dead = true;
            }
        }        
    }
    public void ResetHealthComponent()
    {
        currentHP = maxHP;
        dead = false;
    }
}
