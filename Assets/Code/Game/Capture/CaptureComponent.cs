﻿using GameEnum;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CaptureComponent : MonoBehaviour
{
    [SerializeField] private FactionAlignmentType factionAlignmentType;
    public FactionAlignmentType FactionAlignmentType { get => factionAlignmentType; set => factionAlignmentType = value; }

}
