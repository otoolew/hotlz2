﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerBehaviour : UnitActor
{
    [SerializeField] private Rigidbody rigidBody;
    public override Rigidbody RigidBody { get => rigidBody; set => rigidBody = value; }

    [SerializeField] private FactionComponent factionComponent;
    public override FactionComponent FactionComponent { get => factionComponent; set => factionComponent = value; }

    //[SerializeField] private HealthComponent healthComponent;
    //public override HealthComponent HealthComponent { get => healthComponent; set => healthComponent = value; }

    //[SerializeField] private Transform targetPoint;
    //public override Transform TargetPoint { get => targetPoint; set => targetPoint = value; }

    [SerializeField] private TowerTurret towerTurret;
    public TowerTurret TowerTurret { get => towerTurret; set => towerTurret = value; }

    [SerializeField] private TargettingComponent targettingComponent;
    public TargettingComponent TargettingComponent { get => targettingComponent; set => targettingComponent = value; }

    [SerializeField] private Timer reviveTimer;
    public Timer ReviveTimer { get => reviveTimer; set => reviveTimer = value; }

    [SerializeField] private ParticleEffect smokePFX;
    public ParticleEffect SmokePFX { get => smokePFX; set => smokePFX = value; }

    //public override event Action<UnitActor> Removed;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //if (HealthComponent.Dead)
        //{
        //    reviveTimer.RunTimer();
        //    if (reviveTimer.Ready)
        //    {
        //        reviveTimer.ResetTimer();
        //        Revive();
        //    }
        //}
        //else
        //{
        //    if (targettingComponent.CurrentTarget != null)
        //    {
        //        towerTurret.AimAt(targettingComponent.CurrentTarget.transform.position);
        //        if (towerTurret.WeaponComponent.WeaponReady)
        //        {
        //            towerTurret.WeaponComponent.Fire();
        //        }
        //    }
        //}
    }
    public void ResetDefenseTower()
    {
        targettingComponent.ResetTargetter();
    }

    public override void OnRemoved()
    {
        Debug.Log("DefenseTower Destroyed");
        ResetDefenseTower();
        //Removed?.Invoke(this);
    }
    public void Revive()
    {
        smokePFX.Deactivate();
        //HealthComponent.ResetHealthComponent();
        //targettingComponent.enabled = true;
    }
    public void OnDeath()
    {
        OnRemoved();
        //targettingComponent.enabled = false;
        smokePFX.Activate();
    }
}
