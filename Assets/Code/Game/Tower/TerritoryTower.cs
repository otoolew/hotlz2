﻿using System;
using UnityEngine;

public class TerritoryTower : UnitActor
{
    [SerializeField] private Rigidbody rigidBody;
    public override Rigidbody RigidBody { get => rigidBody; set => rigidBody = value; }

    [SerializeField] private FactionComponent factionComponent;
    public override FactionComponent FactionComponent { get => factionComponent; set => factionComponent = value; }

    //[SerializeField] private HealthComponent healthComponent;
    //public override HealthComponent HealthComponent { get => healthComponent; set => healthComponent = value; }

    //[SerializeField] private Transform targetPoint;
    //public override Transform TargetPoint { get => targetPoint; set => targetPoint = value; }

    [SerializeField] private Territory residingTerritory;
    public Territory ResidingTerritory { get => residingTerritory; set => residingTerritory = value; }

    [SerializeField] private TargettingComponent targettingComponent;
    public TargettingComponent TargettingComponent { get => targettingComponent; set => targettingComponent = value; }

    [SerializeField] private WeaponComponent weaponComponent;
    public WeaponComponent WeaponComponent { get => weaponComponent; set => weaponComponent = value; }

    [SerializeField] private Transform turretBase;
    public Transform TurretBase { get => turretBase; set => turretBase = value; }

    [SerializeField] private Transform firePoint;
    public Transform FirePoint { get => firePoint; set => firePoint = value; }

    [SerializeField] private Timer reviveTimer;
    public Timer ReviveTimer { get => reviveTimer; set => reviveTimer = value; }

    [SerializeField] private LayerMask layerMask;
    public LayerMask LayerMask { get => layerMask; set => layerMask = value; }

    [SerializeField] private ParticleEffect smokePFX;
    public ParticleEffect SmokePFX { get => smokePFX; set => smokePFX = value; }

    [SerializeField] private MaterialSwap materialSwap;
    public MaterialSwap MaterialSwap { get => materialSwap; set => materialSwap = value; }

    //public override event Action<UnitActor> Removed;


    public void InitComponents()
    {
        if (FactionComponent == null)
            FactionComponent = GetComponent<FactionComponent>();
        //if (HealthComponent == null)
        //    HealthComponent = GetComponent<HealthComponent>();
        if (TargettingComponent == null)
            TargettingComponent = GetComponentInChildren<TargettingComponent>();
        if (WeaponComponent == null)
            WeaponComponent = GetComponentInChildren<WeaponComponent>();
    }

    //Start is called before the first frame update
    private void Start()
    {
        InitComponents();
        targettingComponent.FactionData = FactionComponent.factionData;
        ResetDefenseTower();
    }

    // Update is called once per frame
    private void Update()
    {
        //if (HealthComponent.Dead)
        //{
        //    reviveTimer.RunTimer();
        //    if (reviveTimer.Ready)
        //    {
        //        reviveTimer.ResetTimer();
        //        Revive();
        //    }
        //}
        //else
        //{
        //    if (targettingComponent.CurrentTarget != null)
        //    {
        //        AimTurretBase();
        //        AimTurretWeapon();
        //        if (WeaponComponent.WeaponReady)
        //        {
        //            FireWeapon();
        //        }
        //    }
        //}

    }
    public void AimTurretBase()
    {
        Vector3 targetDir = targettingComponent.CurrentTarget.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(targetDir);
        lookRotation.x = 0f;
        lookRotation.z = 0f;
        turretBase.rotation = lookRotation;
    }
    private void AimTurretWeapon()
    {
        //weaponComponent.transform.LookAt(targettingComponent.CurrentTarget.TargetPoint.position);
    }

    private void FireWeapon()
    {
        if (targettingComponent.CurrentTarget == null)
            return;
        weaponComponent.Fire();
    }

    public void ChangeFactionOwnership(FactionData newFaction)
    {
        factionComponent.factionData = newFaction;
        targettingComponent.FactionData = newFaction;
        //materialSwap.Swap(newFaction.FactionMaterial.flagMaterial);
        targettingComponent.ResetTargetter();
    }

    public void ResetDefenseTower()
    {
        targettingComponent.ResetTargetter();
    }

    public override void OnRemoved()
    {
        Debug.Log("DefenseTower Destroyed");
        ResetDefenseTower();
        //Removed?.Invoke(this);
    }
    public void Revive()
    {
        residingTerritory.UpdateTowerOwnership();
        smokePFX.Deactivate();
        //HealthComponent.ResetHealthComponent();
    }
    public void OnDeath()
    {
        OnRemoved();
        smokePFX.Activate();
    }
}
