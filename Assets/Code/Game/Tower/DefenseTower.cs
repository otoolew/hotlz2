﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenseTower : UnitActor
{
    [SerializeField] private Rigidbody rigidBody;
    public override Rigidbody RigidBody { get => rigidBody; set => rigidBody = value; }

    [SerializeField] private FactionComponent factionComponent;
    public override FactionComponent FactionComponent { get => factionComponent; set => factionComponent = value; }

    //[SerializeField] private DefensePosition[] defensePositions;
    //public DefensePosition[] DefensePositions { get => defensePositions; private set => defensePositions = value; }

    [SerializeField] private TargettingComponent targettingComponent;
    public TargettingComponent TargettingComponent { get => targettingComponent; set => targettingComponent = value; }

    [SerializeField] private WeaponComponent weaponComponent;
    public WeaponComponent WeaponComponent { get => weaponComponent; set => weaponComponent = value; }

    [SerializeField] private Transform turretBase;
    public Transform TurretBase { get => turretBase; set => turretBase = value; }

    [SerializeField] private Transform firePoint;
    public Transform FirePoint { get => firePoint; set => firePoint = value; }

    [SerializeField] private Timer reviveTimer;
    public Timer ReviveTimer { get => reviveTimer; set => reviveTimer = value; }

    [SerializeField] private LayerMask layerMask;
    public LayerMask LayerMask { get => layerMask; set => layerMask = value; }

    [SerializeField] private ParticleEffect smokePFX;
    public ParticleEffect SmokePFX { get => smokePFX; set => smokePFX = value; }

    [SerializeField] private MaterialSwap materialSwap;
    public MaterialSwap MaterialSwap { get => materialSwap; set => materialSwap = value; }

    public int blueTroopCount;
    public int redTroopCount;

    
    private void LoadDefensePositions()
    {
        //defensePositions = GetComponentsInChildren<DefensePosition>();
        //for (int i = 0; i < defensePositions.Length; i++)
        //{

        //}
    }
    //Start is called before the first frame update
    private void Start()
    {
        LoadDefensePositions();
        ResetDefenseTower();
        UpdateTowerOwnership();
    }

    // Update is called once per frame
    private void Update()
    {
        //if (HealthComponent.Dead)
        //{
        //    reviveTimer.RunTimer();
        //    if (reviveTimer.Ready)
        //    {
        //        reviveTimer.ResetTimer();
        //        Revive();
        //    }
        //}
        //else
        //{
        //    if (targettingComponent.CurrentTarget != null)
        //    {
        //        AimTurretBase();
        //        AimTurretWeapon();
        //        FireWeapon();
        //    }
        //}

    }
    public void AimTurretBase()
    {
        Vector3 targetDir = targettingComponent.CurrentTarget.transform.position - transform.position;
        Quaternion lookRotation = Quaternion.LookRotation(targetDir);
        lookRotation.x = 0f;
        lookRotation.z = 0f;
        turretBase.rotation = lookRotation;
    }
    private void AimTurretWeapon()
    {
        //aponComponent.transform.LookAt(targettingComponent.CurrentTarget.TargetPoint.position);
    }

    private void FireWeapon()
    {
        if (targettingComponent.CurrentTarget == null)
            return;

        Ray ray = new Ray
        {
            origin = firePoint.transform.position,
            direction = firePoint.transform.forward
        };

        if (Physics.Raycast(ray, out RaycastHit rayHit, layerMask))
        {
            var targetUnit = rayHit.collider.GetComponentInParent<UnitActor>();
            if (targetUnit != null)
            {
                //ctor3 targetDir = targetUnit.TargetPoint.position - firePoint.transform.position;
                weaponComponent.Fire();
            }
        }
    }


    public void UpdateTowerOwnership()
    {
        if (blueTroopCount > redTroopCount)
        {
            if(factionComponent.factionData != GameManager.Instance.FactionServer.Blue)
            {
                ChangeFactionOwnership(GameManager.Instance.FactionServer.Blue);
            }

        }
        else if(blueTroopCount < redTroopCount)
        {
            if (factionComponent.factionData != GameManager.Instance.FactionServer.Red)
            {
                ChangeFactionOwnership(GameManager.Instance.FactionServer.Red);
            }
        }
        else
        {
            if (factionComponent.factionData != GameManager.Instance.FactionServer.Neutral)
            {
                ChangeFactionOwnership(GameManager.Instance.FactionServer.Neutral);
            }
        }
    }
    public void ChangeFactionOwnership(FactionData newFaction)
    {
        factionComponent.factionData = newFaction;
        targettingComponent.FactionData = newFaction;
        //materialSwap.Swap(newFaction.colorMaterial);
        targettingComponent.ResetTargetter();
    }

    public void ResetDefenseTower()
    {
        targettingComponent.ResetTargetter();
    }

    public override void OnRemoved()
    {
        Debug.Log("DefenseTower Destroyed");
        ResetDefenseTower();
        //Removed?.Invoke(this);
    }
    public void Revive()
    {
        smokePFX.Deactivate();
        //HealthComponent.ResetHealthComponent();
        //targettingComponent.enabled = true;
        UpdateTowerOwnership();
    }
    public void OnDeath()
    {
        OnRemoved();
        //targettingComponent.enabled = false;
        smokePFX.Activate();
    }
}
