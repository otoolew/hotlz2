﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TowerTurret : MonoBehaviour
{
    [SerializeField] private float rotationSpeed;
    public float RotationSpeed { get => rotationSpeed; set => rotationSpeed = value; }

    [SerializeField] private WeaponComponent weaponComponent;
    public WeaponComponent WeaponComponent { get => weaponComponent; set => weaponComponent = value; }

    public void AimAt(Vector3 targetPoint)
    {
        Vector3 targetDir = targetPoint - transform.position;
        Vector3 newDir = Vector3.RotateTowards(transform.forward, targetDir, 10f, 0.0f);
        transform.rotation = Quaternion.LookRotation(targetDir);        
    }
}
