﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvent;

public abstract class Objective : MonoBehaviour
{
    public abstract bool ObjectiveCompleted(SoldierController soldierUnit);
    // Start is called before the first frame update
}
