﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CaptureObjective : Objective
{
    public CaptureComponent[] captureComponents;

    private void Start()
    {
        captureComponents = GetComponentsInChildren<CaptureComponent>();
    }
    public override bool ObjectiveCompleted(SoldierController soldierUnit)
    {
        for (int i = 0; i < captureComponents.Length; i++)
        {
            if ((captureComponents[i].FactionAlignmentType == GameEnum.FactionAlignmentType.NONE) || (captureComponents[i].FactionAlignmentType != soldierUnit.FactionData.FactionAlignmentType))
            {
                soldierUnit.soldierNavigation.MoveToPosition(captureComponents[i].transform.position);
                return false;
            }
        }
        return true;
    }

}
