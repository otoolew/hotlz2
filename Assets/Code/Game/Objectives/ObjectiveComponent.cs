﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class ObjectiveComponent : MonoBehaviour
{
    public Objective blueObjective;
    public Objective redObjective;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void CheckObjectiveCompletion(SoldierController soldierUnit)
    {
        switch (soldierUnit.FactionData.FactionAlignmentType)
        {
            case GameEnum.FactionAlignmentType.NONE:
                break;
            case GameEnum.FactionAlignmentType.BLUE:
                blueObjective.ObjectiveCompleted(soldierUnit);
                break;
            case GameEnum.FactionAlignmentType.RED:
                break;
        }
    }
}