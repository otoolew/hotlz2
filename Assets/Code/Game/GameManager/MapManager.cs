﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapManager : MonoBehaviour
{
    [SerializeField] private FactionManager blueFactionManager;
    public FactionManager BlueFactionManager { get => blueFactionManager; set => blueFactionManager = value; }

    [SerializeField] private FactionManager redFactionManager;
    public FactionManager RedFactionManager { get => redFactionManager; set => redFactionManager = value; }

}
