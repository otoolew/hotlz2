﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameManager : Singleton<GameManager>
{
    /// <summary>
    /// Data container for storing persistent game data across scenes
    /// </summary>
    [SerializeField] private GameData gameData;
    public GameData GameData { get => gameData; set => gameData = value; }
    /// <summary>
    /// Enum to store our game manager instances state
    /// </summary>
    public enum GameState { RUNNING, PAUSED, SCENECHANGE, GAMEOVER }
    /// <summary>
    /// GameState enum to store GameState
    /// </summary>
    [SerializeField] GameState currentGameState = GameState.RUNNING;
    public GameState CurrentGameState { get => currentGameState; private set => currentGameState = value; }

    [SerializeField] private FactionServer factionServer;
    public FactionServer FactionServer { get => factionServer; set => factionServer = value; }

    /// <summary>
    /// Component for controlling all scene related functionality
    /// </summary>
    [SerializeField] private SceneController sceneController;
    public SceneController SceneController { get => sceneController; set => sceneController = value; }

    /// <summary>
    /// Event that will notify all subscribers of the GameStateChange Event.
    /// </summary>
    public GameStateChange OnGameStateChanged;
  
    /// <summary>
    /// Execute after Awake and before first Update. Init Here!
    /// </summary>
    void Start()
    {
        DontDestroyOnLoad(gameObject); // Tells Unity that we do not want to destroy this GameObject on Scene Load
        GameData.CurrentGameState = currentGameState; // Set the GameManager state to the stores state in GMData
        sceneController = GetComponent<SceneController>();
        sceneController.onSceneChangeStart.AddListener(HandleSceneChangeStart); // Subscribe or Listen for EventSceneChangeStart
        sceneController.onSceneChangeComplete.AddListener(HandleSceneChangeComplete);// Subscribe or Listen for EventSceneChangeComplete
        OnGameStateChanged?.Invoke(GameData.CurrentGameState, currentGameState); // Let ALL other components listening for EventGameStateChange that the GameState has changed
    }
    /// <summary>
    /// Called every frame.
    /// </summary>
    void Update()
    {
        // If scene is changing return
        if (currentGameState == GameState.SCENECHANGE)
            return;
    }

    /// <summary>
    /// Updates GameState. Is call when another script triggers a state change. Can also be called directly.
    /// </summary>
    /// <param name="state"></param>
    public void UpdateState(GameState state)
    {
        GameState previousGameState = currentGameState; // store previous state
        currentGameState = state; // set current state to the new state
        GameData.CurrentGameState = currentGameState; // store the current state in our persistent data container

        // Switch to handle what needs to execute for each state 
        switch (CurrentGameState)
        {
            case GameState.SCENECHANGE:
                // Initialize any systems that need to be reset
                Debug.Log("Scene Changing");
                Time.timeScale = 1.0f; // Time.timeScale will slow down time or bring it to a complete stop. 
                break;

            case GameState.RUNNING:
                //  Unlock player, enemies and input in other systems, update tick if you are managing time
                Debug.Log("Game Running");
                Time.timeScale = 1.0f;
                break;

            case GameState.PAUSED:
                // Pause player, enemies etc, Lock other input in other systems
                Debug.Log("Game Paused");
                Time.timeScale = 0.0f; // Time.timeScale will stop the game. Paused!
                break;
            case GameState.GAMEOVER:
                // Implement Actions like - Pause player, enemies etc, Lock other input in other systems. 
                Debug.Log("Game Over");
                Time.timeScale = 1.0f;
                break;

            default:
                break;
        }
        // When this executes it will Notify ALL Scripts that are subscribed or listening for the EventGameState
        OnGameStateChanged?.Invoke(currentGameState, previousGameState);
    }
    /// <summary>
    /// Toggles between the PAUSED State and the RUNNING State
    /// </summary>
    public void TogglePause()
    {
        //The ? conditional operator commonly known as the ternary conditional operator, returns one of two values depending on the value of a Boolean expression.
        UpdateState(currentGameState == GameState.RUNNING ? GameState.PAUSED : GameState.RUNNING);
        // Verbose version of the code above.
        //if (_currentGameState == GameState.RUNNING)
        //    UpdateState(GameState.PAUSED);
        //else
        //    UpdateState(GameState.RUNNING);
    }
    /// <summary>
    /// Loads the first game level
    /// </summary>
    public void StartGame()
    {
        /// This can be implemented with a Dictionary, or many other more robust ways.
        sceneController.FadeAndLoadScene(sceneController.Scenes[1].SceneName);
        // <-----> Simple way using stored variable declared above, prone to error if string value is a typo
        // sceneController.FadeAndLoadScene(gameScene);

        // <-----> Hard coded way prone to error if string value is a typo
        // sceneController.FadeAndLoadScene("gameScene");

    }
    /// <summary>
    /// Restarts the current level
    /// </summary>
    public void RestartLevel()
    {
        sceneController.FadeAndLoadScene(sceneController.CurrentScene);
    }
    /// <summary>
    /// Quits the game and changes the scene to the TitleMenu
    /// </summary>
    public void QuitToTitle()
    {
        sceneController.FadeAndLoadScene(sceneController.Scenes[0].SceneName);

        // <-----> Simple way using stored variable declared above, prone to error if string value is a typo
        // sceneController.FadeAndLoadScene(titleScene);

        // <-----> Hard coded way prone to error if string value is a typo
        // sceneController.FadeAndLoadScene("TitleScene");
    }
    /// <summary>
    /// Quits the application.
    /// </summary>
    public void QuitGame()
    {
        //These are called regions. When the C# compiler encounters an #if directive, 
        //  followed eventually by an #endif directive, it compiles the code between the 
        //  directives only if the specified symbol is defined. 
#if UNITY_STANDALONE //If we are running in a standalone build of the game
        //Quit the application
        Application.Quit();
#endif

        //If we are running in the editor
#if UNITY_EDITOR
        //Stop playing the scene.
        UnityEditor.EditorApplication.isPlaying = false;
#endif      
    }
    /// <summary>
    /// When the event EventSceneChangeStart fires, this method is executed.
    /// </summary>
    /// <param name="started"></param>
    public void HandleSceneChangeStart(bool started)
    {
        Debug.Log("[GameManager] Scene Change Start.");
        UpdateState(GameState.SCENECHANGE);
    }
    /// <summary>
    /// When the event EventSceneChangeComplete fires, this method is executed.
    /// </summary>
    public void HandleSceneChangeComplete(bool complete)
    {
        Debug.Log("[GameManager] Scene Change Complete.");
        UpdateState(GameState.RUNNING); // The Scene change is complete so set the game state to run.
    }

    #region Subclass
    /// <summary>
    /// Subclass EventGameState using scoped enum.  
    /// </summary>
    [Serializable] public class GameStateChange : UnityEvent<GameState, GameState> { }
    #endregion
}
