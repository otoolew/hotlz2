﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using GameEnum;
using GameEvent;

public class Garrison : MonoBehaviour
{
    /// <summary>
    /// Enum to store our game manager instances state
    /// </summary>
    public enum GarrisonState { OPEN, CLOSED }
    /// <summary>
    /// GameState enum to store GameState
    /// </summary>
    [SerializeField] GarrisonState currentGarrisonState = GarrisonState.OPEN;
    public GarrisonState CurrentGarrisonState { get => currentGarrisonState; private set => currentGarrisonState = value; }

    [SerializeField] private FactionAlignmentType factionAlignmentType;
    public FactionAlignmentType FactionAlignmentType { get => factionAlignmentType; set => factionAlignmentType = value; }

    [SerializeField] private CaptureComponent captureComponent;
    public CaptureComponent CaptureComponent { get => captureComponent; set => captureComponent = value; }

    [SerializeField] private Transform garrisonExit;
    public Transform GarrisonExit { get => garrisonExit; set => garrisonExit = value; }

    [SerializeField] private SoldierController currentOccupant;
    public SoldierController CurrentOccupant { get => currentOccupant; set => currentOccupant = value; }

    [SerializeField] private List<SoldierController> soldierOverflowList;
    public List<SoldierController> SoldierOverflowList { get => soldierOverflowList; set => soldierOverflowList = value; }
    public GarrisonClose GarrisonClosed;

    public GarrisonStateChange GarrisonStateChanged;

    public bool Empty()
    {
        if (currentOccupant == null)
            return true;
        else
            return false;
    }
    public bool Full()
    {
        if (currentOccupant == null)
            return true;
        else
            return false;
    }
    // Start is called before the first frame update
    void Start()
    {
        soldierOverflowList = new List<SoldierController>();
        //garrisonedUnitList = new List<SoldierUnit>();

    }

    // Update is called once per frame
    void Update()
    {

    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger || other.transform.root.GetComponent<SoldierController>() == null)
            return;
        SoldierController soldierUnit = other.transform.root.GetComponent<SoldierController>();
        if (CurrentGarrisonState == GarrisonState.CLOSED || CurrentOccupant != null)
        {
            //soldierUnit.RequestDutyAssignment();
            return;
        }

        GarrisonSoldier(soldierUnit);
    }
    private void GarrisonSoldier(SoldierController soldierUnit)
    {
        UpdateState(GarrisonState.CLOSED);
        //soldierUnit.Garrisoned = true;
        soldierUnit.RemoveUnit();
        CurrentOccupant = soldierUnit;
        CaptureComponent.FactionAlignmentType = soldierUnit.FactionData.FactionAlignmentType;
    }

    public void UpdateState(GarrisonState state)
    {
        GarrisonState previousGarrisonState = currentGarrisonState; // store previous state
        currentGarrisonState = state; // set current state to the new state

        // Switch to handle what needs to execute for each state 
        switch (currentGarrisonState)
        {
            case GarrisonState.OPEN:
                // Initialize any systems that need to be reset
                Debug.Log("Garrison Open");
                CurrentOccupant = null;
                //GetComponent<Collider>().enabled = true;
                CaptureComponent.FactionAlignmentType = FactionAlignmentType.NONE;
                break;
            case GarrisonState.CLOSED:
                // Initialize any systems that need to be reset
                Debug.Log("Garrison Closed");
                //GetComponent<Collider>().enabled = false;
                GarrisonClosed.Invoke(this);
                break;
            default:
                break;
        }
        // When this executes it will Notify ALL Scripts that are subscribed or listening for the EventGameState
        GarrisonStateChanged?.Invoke(this);
    }

    private void OnOccupantRemoved(Damageable obj)
    {
        obj.Removed -= OnOccupantRemoved;
        SoldierController soldierUnit = obj.GetComponent<SoldierController>();
        //soldierUnit.Garrisoned = false;
        UpdateState(GarrisonState.OPEN);
    }

    #region Subclass
    /// <summary>
    /// Subclass EventGameState using scoped enum.  
    /// </summary>
    [Serializable] public class GarrisonStateChange : UnityEvent<Garrison> { }

    #endregion
}
