﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Headquarters : Building
{
    [SerializeField] private Transform entranceTransform;
    public Transform EntranceTransform { get => entranceTransform; set => entranceTransform = value; }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        SoldierController soldierUnit = other.transform.root.GetComponent<SoldierController>();
        if (soldierUnit == null)
            return;

    }
}
