﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Building : Targetable
{
    [SerializeField] private Territory currentTerritory;
    public Territory CurrentTerritory { get => currentTerritory; set => currentTerritory = value; }

    [SerializeField] private Transform entryPoint;
    public Transform EntryPoint { get => entryPoint; set => entryPoint = value; }

    [SerializeField] private bool destroyed;
    public bool Destroyed { get => destroyed; set => destroyed = value; }

    protected override void Awake()
    {
        base.Awake();
        Died += OnDeath;
    }
    //private void OnTriggerEnter(Collider other)
    //{
    //    SoldierUnit soldier = other.transform.root.GetComponent<SoldierUnit>();
    //    Debug.Log(soldier);
    //    if (soldier == null)
    //        return;
    //}
    private void OnDeath(Damageable obj)
    {
        Debug.Log(obj.name + " Structure Destroyed");
        Remove();
        Destroyed = true;
        StartCoroutine("DeathSequence");
    }

    IEnumerator DeathSequence()
    {
        yield return new WaitForSeconds(2.5f);
        gameObject.SetActive(false);
    }
}
