﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BuildingEntry : MonoBehaviour
{
    Building Building { get; set; }
    // Start is called before the first frame update
    void Start()
    {
        Building = GetComponent<Building>();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter(Collider other)
    {
        SoldierController soldier = other.transform.root.GetComponent<SoldierController>();
        Debug.Log(soldier);
        if (soldier == null)
            return;

        if (Building.FactionData == soldier.FactionData)
            return;
        else
            Debug.Log("Enemy Entered Base");
    }
}
