﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Barracks : Building
{
    [SerializeField] private FactionManager factionManager;
    public FactionManager FactionManager { get => factionManager; set => factionManager = value; }

    [SerializeField] private Timer troopSpawnTimer;
    public Timer TroopSpawnTimer { get => troopSpawnTimer; set => troopSpawnTimer = value; }

    // Start is called before the first frame update
    void Start()
    {
        troopSpawnTimer.CountdownFinished += SpawnSoldier;
    }

    private void SpawnSoldier()
    {
        GameObject soldier = factionManager.SoldierPool.FetchFromPool();
        if (soldier == null)
            return;

        soldier.SetActive(true);
        soldier.transform.parent = null;
        soldier.GetComponent<SoldierController>().Pooled = false;
        soldier.GetComponent<SoldierNavigation>().CurrentTerritory = CurrentTerritory;
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
