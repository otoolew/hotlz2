﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using GameEnum;

public abstract class Navigator : MonoBehaviour
{
    public abstract NavMeshAgent NavAgent { get; set; }
    public abstract Territory CurrentTerritory { get; set; }
    public abstract DestinationArea CurrentDestinationArea { get; set; }
    public abstract bool FindNextDestination();
    public abstract void MoveToPosition(Vector3 newPosition);
    public abstract bool ArrivedAtDestination { get; set; }
    public abstract void FindClosestTerritory();

    public float DistanceToDestination()
    {
        return Vector3.Distance(NavAgent.destination, transform.position);
    }

    public void ClearNavAgentPath()
    {
        NavAgent.ResetPath();
    }
    public void ActivateNavAgent()
    {
        NavAgent.enabled = true;
        NavAgent.isStopped = false;
    }
    public void DeactivateNavAgent()
    {
        NavAgent.isStopped = false;
        NavAgent.enabled = false;
    }
}
