﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(DestinationArea))]
public class DestinationEditor : Editor
{
    protected DestinationArea Destination;

    protected void OnEnable()
    {
        Destination = (DestinationArea)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        GUILayout.Space(5);
        if (GUILayout.Button("Select/Add Mesh"))
        {
            AddMeshCreator();
        }
    }

    /// <summary>
    /// Creates a new AreaMeshCreator object as a child of the node
    /// </summary>
    protected void AddMeshCreator()
    {
        var meshObject = Destination.GetComponentInChildren<AreaMeshCreator>();

        // AreaMeshCreatorObject already exists so no need to instantiate it again
        if (meshObject != null)
        {
            Selection.activeGameObject = meshObject.gameObject;
            return;
        }

        GameObject newGameObject = new GameObject("AreaMesh");
        newGameObject.transform.SetParent(Destination.transform, false);
        meshObject = newGameObject.AddComponent<AreaMeshCreator>();

        Selection.activeGameObject = meshObject.gameObject;
        Undo.RegisterCreatedObjectUndo(meshObject.gameObject, "Created AreaMeshCreator");
    }
}
