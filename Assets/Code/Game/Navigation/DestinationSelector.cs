﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnum;

public class DestinationSelector : MonoBehaviour
{
    public DestinationSelectMode DestinationSelectMode { get; set; }
    public List<DestinationArea> ConnectedDestinationList;

    protected virtual void Start()
    {
        ConnectedDestinationList = new List<DestinationArea>();
    }
    public virtual DestinationArea GetNextDestination()
    {
        if (ConnectedDestinationList.Count > 0)
        {
            return ConnectedDestinationList[0];
        }
        return null;
    }
    
#if UNITY_EDITOR
    /// <summary>
    /// Draws the links between nodes for editor purposes
    /// </summary>
    protected virtual void OnDrawGizmos()
    {
        if (ConnectedDestinationList == null)
        {
            return;
        }
        int blueCount = ConnectedDestinationList.Count;
        for (int i = 0; i < blueCount; i++)
        {
            DestinationArea destination = ConnectedDestinationList[i];
            if (destination != null)
            {
                Gizmos.DrawLine(transform.position, destination.transform.position);
            }
        }
    }
#endif
}
