﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FixedDestinationSelector : DestinationSelector
{
    /// <summary>
    /// Selects the next destination
    /// </summary>
    /// <returns>The next selected destination, or null if there are no valid destinations</returns>
    public override DestinationArea GetNextDestination()
    {
        //if (connectedDestinationList.Count > 0)
        //{
        //    return connectedDestinationList[0];
        //}       
        return null;
    }

#if UNITY_EDITOR
    protected override void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        base.OnDrawGizmos();
    }
#endif
}
