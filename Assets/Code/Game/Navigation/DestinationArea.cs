﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestinationArea : MonoBehaviour
{
    public string DestinationAreaID;
    public Territory residingTerritory;

    /// <summary>
    /// Reference to the MeshObject created by an AreaMeshCreator
    /// </summary>
    [HideInInspector]
    public AreaMeshCreator areaMesh;

    /// <summary>
    /// Gets the next node from the selector
    /// </summary>
    /// <returns>Next node, or null if this is the terminating node</returns>
    public DestinationArea GetNextDestination()
    {
        var selector = GetComponent<DestinationSelector>();
        if (selector != null)
        {
            return selector.GetNextDestination();
        }
        return null;
    }

    /// <summary>
    /// Gets a random point inside the area defined by a node's meshcreator
    /// </summary>
    /// <returns>A random point within the MeshObject's area</returns>
    public Vector3 GetRandomPointInArea()
    {
        // Fallback to our position if we have no mesh
        return areaMesh == null ? transform.position : areaMesh.GetRandomPointInside();
    }

    ///// <summary>
    ///// When agent enters the node area, get the next node
    ///// </summary>
    //public virtual void OnTriggerEnter(Collider other)
    //{
    //    if (other.isTrigger)
    //        return;
    //    var navigator = other.gameObject.transform.root.GetComponent<Navigator>();
    //    if (navigator != null)
    //    {
    //        navigator.GetNextNode();
    //        soldier.UnitNavigation.GetNextNode(this);
    //        soldier.UnitNavigation.MoveToCurrentNode();
    //    }
    //}

#if UNITY_EDITOR
    /// <summary>
    /// Ensure the collider is a trigger
    /// </summary>
    protected void OnValidate()
    {
        //var trigger = GetComponent<Collider>();
        //if (trigger != null)
        //{
        //    trigger.isTrigger = true;
        //}

        // Try and find AreaMeshCreator
        if (areaMesh == null)
        {
            areaMesh = GetComponentInChildren<AreaMeshCreator>();
        }
    }

    void OnDrawGizmos()
    {
        Gizmos.DrawIcon(transform.position + Vector3.up, "movement_node.png", true);
    }
#endif
}
