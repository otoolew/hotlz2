﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Destination
{
    public string destinationID;
    public Vector3 destinationLocation;
}
