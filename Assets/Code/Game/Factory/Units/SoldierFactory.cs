﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newSoldierFactory", menuName = "Factory/Soldier Factory")]
public class SoldierFactory : Factory
{
    public SoldierData soldierData;
    public override ScriptableObject ObjectData => soldierData;

    public override GameObject Create(Pool pool)
    {
        GameObject soldier = Instantiate(soldierData.soldierPrefab, pool.transform);
        soldierData.InitializeSoldier(soldier.GetComponent<SoldierController>());
        soldier.GetComponent<SoldierController>().SoldierPool = pool.GetComponent<SoldierPool>();
        soldier.GetComponent<IPoolable>().Pooled = true;
        soldier.SetActive(false);
        return soldier;
    }

}
