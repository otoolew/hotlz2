﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Factory : ScriptableObject
{
    public abstract ScriptableObject ObjectData { get; }
    public abstract GameObject Create(Pool pool);
}
