﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newHomingRocketFactory", menuName = "Factory/Munitions/Homing Rocket")]
public class HomingRocketFactory : Factory
{
    public HomingRocketMunitonData homingRocketData;
    public override ScriptableObject ObjectData { get => homingRocketData; }

    public override GameObject Create(Pool munitionPool)
    {
        GameObject rocket = Instantiate(homingRocketData.munitionPrefab, munitionPool.transform);
        homingRocketData.InitializeMunition(rocket.GetComponent<HomingRocketMunition>());

        rocket.GetComponent<HomingRocketMunition>().MunitionPool = munitionPool.GetComponent<MunitionPool>();
        rocket.GetComponent<HomingRocketMunition>().Pooled = true;
        rocket.SetActive(false);
        return rocket;
    }
}
