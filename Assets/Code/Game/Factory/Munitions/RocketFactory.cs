﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newRocketFactory", menuName = "Factory/Munitions/Rocket")]
public class RocketFactory : Factory
{
    public RocketMunitonData rocketMunitonData;
    public override ScriptableObject ObjectData { get => rocketMunitonData;}

    public override GameObject Create(Pool munitionPool)
    {
        GameObject rocket = Instantiate(rocketMunitonData.munitionPrefab, munitionPool.transform);
        rocketMunitonData.InitializeMunition(rocket.GetComponent<RocketMunition>());
        rocket.GetComponent<RocketMunition>().MunitionPool = munitionPool.GetComponent<MunitionPool>();
        rocket.GetComponent<RocketMunition>().Pooled = true;
        rocket.SetActive(false);
        return rocket;
    }
}
