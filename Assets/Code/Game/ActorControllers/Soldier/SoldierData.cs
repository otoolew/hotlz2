﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newSoldierData", menuName = "Units/Soldier Data")]
public class SoldierData : ScriptableObject
{
    public FactionData factionData;
    public string soldierName;
    public GameObject soldierPrefab;

    public void InitializeSoldier(SoldierController soldier)
    {
        soldier.name = soldierName;
        soldier.FactionData = factionData;
    }
}

