﻿using GameEnum;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.Events;

[RequireComponent(typeof(NavMeshAgent))]
public class SoldierNavigation : Navigator
{
    #region Fields and Properties

    [SerializeField] private NavMeshAgent navAgent;
    public override NavMeshAgent NavAgent { get => navAgent; set => navAgent = value; }

    [SerializeField] private Territory currentTerritory;
    public override Territory CurrentTerritory { get => currentTerritory; set => currentTerritory = value; }

    public List<Territory> VisitedTerritoryList;

    [SerializeField] private DestinationArea previousDestination;
    public DestinationArea PreviousDestination { get => previousDestination; set => previousDestination = value; }

    [SerializeField] private DestinationArea currentDestinationArea;
    public override DestinationArea CurrentDestinationArea { get => currentDestinationArea; set => currentDestinationArea = value; }

    [SerializeField] private bool arrivedAtDestination;
    public override bool ArrivedAtDestination { get => arrivedAtDestination; set => arrivedAtDestination = value; }
    
    private void Awake()
    {
        navAgent = GetComponent<NavMeshAgent>();
        VisitedTerritoryList = new List<Territory>();
    }
    private void OnEnable()
    {
        ActivateNavAgent();
        //currentDestinationPoint = navAgent.destination;
    }
    #endregion
    // Start is called before the first frame update
    void Start()
    {
        navAgent = GetComponent<NavMeshAgent>();
        ActivateNavAgent();
        if(CurrentDestinationArea == null)
            FindClosestTerritory();
    }

    // Update is called once per frame
    void Update()
    {
        //NOTHING IN UPDATE!
    }

    /// <summary>
    /// The logic for what happens when the destination is reached
    /// </summary>
    public bool DestinationReached()
    {
        arrivedAtDestination = false;
        if (DistanceToDestination() <= NavAgent.stoppingDistance)
            arrivedAtDestination = true;
        else
            arrivedAtDestination = false;
        return arrivedAtDestination;
    }

    public override bool FindNextDestination()
    {
        DestinationArea nextDestination = CurrentDestinationArea.GetNextDestination();

        if (nextDestination != null)
        {
            previousDestination = CurrentDestinationArea;
            CurrentDestinationArea = nextDestination;            
            return true;
        }
        return false;
    }
    public void MoveToCurrentDestination()
    {
        if (!navAgent.isActiveAndEnabled || !gameObject.activeSelf)
            return;
        if (navAgent.isOnNavMesh)
        {
            navAgent.isStopped = false;
            if (CurrentDestinationArea)
            {
                Debug.Log(CurrentDestinationArea.name);
                navAgent.SetDestination(CurrentDestinationArea.areaMesh.GetRandomPointInside());
            }

        }
    }
    public override void MoveToPosition(Vector3 newPosition)
    {
        if (!navAgent.isActiveAndEnabled)
            return;
        if (navAgent.isOnNavMesh)
        {
            navAgent.isStopped = false;
            navAgent.SetDestination(newPosition);
        }
    }

    public override void FindClosestTerritory()
    {
        CurrentTerritory = TerritoryManager.Instance.FindClosestTerritory(this);
    }
}
