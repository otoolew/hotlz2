﻿using Core.StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class SoldierController : UnitController, IPoolable
{
    public FactionData FactionData;
    public Animator animator;
    public SoldierNavigation soldierNavigation;
    public TargettingComponent TargettingComponent;

    public SoldierPool SoldierPool { get; set; }
    public bool Pooled { get; set; }

    // Start is called before the first frame update
    void Start()
    {
        InitializeComponent();
        if (CurrentState != null)
            CurrentState.OnStateEnter(this);
    }

    // Update is called once per frame
    protected override void Update()
    {
        base.Update();

    }

    protected override void ChangeState(State freshState)
    {
        base.ChangeState(freshState);

    }
    private void InitializeComponent()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        if (soldierNavigation == null)
            soldierNavigation = GetComponent<SoldierNavigation>();
    }

    public void Repool()
    {
        Pooled = true;
    }
}
