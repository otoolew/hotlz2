﻿using Core.StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "State Machine/Unit/Contact Move State")]
public class SoldierContactMoveState : State
{
    public override void OnStateEnter(StateMachine stateMachine)
    {

    }
    public override void OnStateUpdate(StateMachine stateMachine)
    {

    }

    public override void OnStateExit(StateMachine stateMachine)
    {

    }


}
