﻿using Core.StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newFindNextDestinationAction", menuName = "State Machine/Unit/Find Next Destination")]
public class FindNextDestination : StateAction
{
    public FactionData factionData;

    public void Awake()
    {
        Debug.Log("FindNextDestination StateAction Awake");
    }
    public override void PerformAction(StateMachine stateMachine)
    {
        Navigator navigator = stateMachine.GetComponent<Navigator>();
        if(navigator != null)
        {
            if (navigator.FindNextDestination())
            {
                navigator.MoveToPosition(navigator.CurrentDestinationArea.GetRandomPointInArea());
            }
            else
            {
                navigator.MoveToPosition(navigator.CurrentDestinationArea.GetRandomPointInArea());
            }
        }
            
    }

}
