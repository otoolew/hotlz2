﻿using Core.StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="State Machine/Unit/Idle State")]
public class SoldierIdleState : State
{

    public override void OnStateEnter(StateMachine stateMachine)
    {
        stateMachine.currentStateUpdateFrequency = updateFrequency;
        Debug.Log(stateMachine.transform.root.gameObject.name + " OnStateEnter");
    }
    public override void OnStateUpdate(StateMachine stateMachine)
    {
        Debug.Log(stateMachine.transform.root.gameObject.name + " OnStateUpdate");
    }

    public override void OnStateExit(StateMachine stateMachine)
    {

    }



}
