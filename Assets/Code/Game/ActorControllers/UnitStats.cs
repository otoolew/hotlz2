﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newUnitStats", menuName = "Units/Unit Stats")]
public class UnitStats : ScriptableObject
{
    public float maxHealth;
    public float maxArmor;
    public float rifleAccuracy;
    public float meleeSkill;
}
