﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public abstract class UnitActor : Targetable
{
    public abstract Rigidbody RigidBody { get; set; }
    public abstract FactionComponent FactionComponent { get; set; }
    public abstract void OnRemoved();
    public Vector3 Velocity => RigidBody.velocity;
}
