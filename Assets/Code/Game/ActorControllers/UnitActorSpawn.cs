﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class UnitActorSpawn : MonoBehaviour
{
    public abstract void SpawnUnit(UnitActor unitActor);
}
