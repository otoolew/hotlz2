﻿using Core.StateMachine;
using GameEvent;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UnitController : StateMachine
{
    public UnitInformation unitInformation;
    public RemoveUnitEvent OnUnitRemoved;

    public void RemoveUnit()
    {
        OnUnitRemoved.Invoke(this);
    } 
}
