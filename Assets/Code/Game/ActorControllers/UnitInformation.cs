﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class UnitInformation 
{
    public FactionData factionData;
    public bool died;
    public bool garrisoned;   
}
