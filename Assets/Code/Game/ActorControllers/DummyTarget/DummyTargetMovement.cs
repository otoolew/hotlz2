﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyTargetMovement : MonoBehaviour
{
    public float speed;
    public Timer changeDirectionTimer;
    public bool left;
    // Start is called before the first frame update
    void Start()
    {
        if (changeDirectionTimer == null)
            changeDirectionTimer = GetComponent<Timer>();
        changeDirectionTimer.CountdownFinished += ChangeDirectionTimer_CountdownFinished;
    }

    private void ChangeDirectionTimer_CountdownFinished()
    {
        changeDirectionTimer.ResetTimer();
        left = left == true ? false : true;
    }

    // Update is called once per frame
    void Update()
    {
        changeDirectionTimer.RunTimer();
        Vector3 direction = Vector3.zero;
        if (left)
            direction = -Vector3.right;
        else
            direction = Vector3.right;
        transform.Translate(direction * Time.deltaTime * speed);
    }

}
