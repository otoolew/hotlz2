﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEvent;

public class FactionComponent : MonoBehaviour
{
    public FactionData factionData;
    public FactionChange FactionChange;

    public void ChangeFactionData(FactionData newFactionData)
    {
        factionData = newFactionData;
        FactionChange.Invoke(newFactionData);
    }
}
