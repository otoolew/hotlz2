﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IFaction
{
    FactionData FactionData {get;set;}
}
