﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnum;

[CreateAssetMenu(fileName = "newFactionData", menuName = "Faction/Alignment")]
public class FactionData : ScriptableObject
{
    public string FactionName;
    public FactionAlignmentType FactionAlignmentType;
    /// <summary>
    /// A collection of other alignment objects that we can harm
    /// </summary>
    public List<FactionData> enemies;

    public bool CanHarm(FactionData other)
    {
        if (other == null)
        {
            return true;
        }
        return other != null && enemies.Contains(other);
    }

}
