﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FactionManager : MonoBehaviour
{
    [SerializeField] private FactionData factionData;
    public FactionData FactionData { get => factionData; set => factionData = value; }

    [SerializeField] private Territory homeTerritory;
    public Territory HomeTerritory { get => homeTerritory; set => homeTerritory = value; }

    [SerializeField] private SoldierPool soldierPool;
    public SoldierPool SoldierPool { get => soldierPool; set => soldierPool = value; }

    [SerializeField] private MunitionPool rocketPool;
    public MunitionPool RocketPool { get => rocketPool; set => rocketPool = value; }

    [SerializeField] private MunitionPool homingRocketPool;
    public MunitionPool HomingRocketPool { get => homingRocketPool; set => homingRocketPool = value; }

    [SerializeField] private Transform soldierSpawnPoint;
    public Transform SoldierSpawnPoint { get => soldierSpawnPoint; set => soldierSpawnPoint = value; }

    [SerializeField] private DestinationArea factionEntry;
    public DestinationArea FactionEntry { get => factionEntry; private set => factionEntry = value; }

    [SerializeField] private Territory[] allTerritories;
    public Territory[] AllTerritories { get => allTerritories; private set => allTerritories = value; }

    private void Start()
    {
        allTerritories = FindObjectsOfType<Territory>();
    }
}
