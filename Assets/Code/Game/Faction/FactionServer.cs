﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newFactionServer", menuName = "Faction/Faction Server")]
public class FactionServer : ScriptableObject
{

    [SerializeField] private FactionData neutral;
    public FactionData Neutral { get => neutral; set => neutral = value; }

    [SerializeField] private FactionData blue;
    public FactionData Blue { get => blue; set => blue = value; }

    [SerializeField] private FactionData red;
    public FactionData Red { get => red; set => red = value; }

}
