﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public abstract class Munition : MonoBehaviour, IPoolable
{
    public abstract MunitionPool MunitionPool { get; set; }
    public abstract float Damage { get; set; }
    public abstract float Speed { get; set; }
    public abstract Timer SelfDestructTimer { get; set; }
    public abstract bool Pooled { get; set; }
    //public abstract void InitializeComponent();
    public abstract void Move();
    public abstract void OnCountdownFinished();
    public abstract void OnContact();
    //protected virtual void Start()
    //{
    //    if (SelfDestructTimer == null)
    //        GetComponent<Timer>();
    //    SelfDestructTimer.ResetTimer();

    //    StartCoroutine(ArmMunition());
    //}

    public IEnumerator ArmMunition()
    {
        yield return new WaitForSeconds(0.2f);
        GetComponent<Collider>().enabled = true; 
    }

    public virtual void Repool()
    {
        Pooled = true;
    }
}