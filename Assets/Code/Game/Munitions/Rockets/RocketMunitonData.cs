﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "newRocketData", menuName = "Munitions/Projectile/Rocket")]
public class RocketMunitonData : MunitionData
{
    public override void InitializeMunition(Munition munition)
    {
        RocketMunition rocketMunition = munition.GetComponent<RocketMunition>();
        rocketMunition.name = munitionName;
        rocketMunition.Damage = damage;
        rocketMunition.Speed = speed;
        rocketMunition.SelfDestructTimer.StartTime = selfDestructTime;
        rocketMunition.SelfDestructTimer.ResetTimer();

    }

}
