﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newHomingRocketData", menuName = "Munitions/Projectile/Homing Rocket")]
public class HomingRocketMunitonData : MunitionData
{
    public float rotationSpeed;
    public override void InitializeMunition(Munition munition)
    {
        HomingRocketMunition homingRocketMunition = munition.GetComponent<HomingRocketMunition>();
        homingRocketMunition.GetComponent<Collider>().enabled = false;
        homingRocketMunition.name = munitionName;
        homingRocketMunition.Damage = damage;
        homingRocketMunition.Speed = speed;
        homingRocketMunition.RotationSpeed = rotationSpeed;
        homingRocketMunition.SelfDestructTimer.StartTime = selfDestructTime;
        homingRocketMunition.SelfDestructTimer.ResetTimer();
        homingRocketMunition.LockedTarget = null;

    }
}
