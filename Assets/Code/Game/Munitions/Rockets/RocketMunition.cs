﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[RequireComponent(typeof(Rigidbody))]
public class RocketMunition : Munition
{
    public override float Damage { get; set; }
    public override float Speed { get; set; }

    [SerializeField] private Timer selfDestructTimer;
    public override Timer SelfDestructTimer { get => selfDestructTimer; set => selfDestructTimer = value; }

    [SerializeField] private MunitionPool munitionPool;
    public override MunitionPool MunitionPool { get => munitionPool; set => munitionPool = value; }

    [SerializeField] private bool pooled;
    public override bool Pooled { get => pooled; set => pooled = value; }

    private void Start()
    {

        StartCoroutine(ArmMunition());
    }
    private void Update()
    {
        SelfDestructTimer.RunTimer();
        Move();
    }
    public override void Move()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * Speed);
    }
    public override void OnCountdownFinished()
    {
        Debug.Log(this.name + " Self Destructed!");
        munitionPool.ReturnToPool(gameObject);
    }

    public override void OnContact()
    {
        //TODO: Play Particles
        munitionPool.ReturnToPool(gameObject);
    }
    private void OnTriggerEnter(Collider other)
    {
        DamageZone damageZone = other.GetComponent<DamageZone>();
        if(damageZone == null)
        {
            Debug.Log("OnTriggerEnter -> "+ this.name + " Hit something with no Damage Zone!");
            OnContact();
        }
        else
        {
            Debug.Log("OnTriggerEnter -> " + this.name + " Hit " + damageZone.transform.root.GetComponent<Damageable>().name);
            damageZone.ApplyDamage(Damage);
            OnContact();
        }

    }
}
