﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Munitions/Projectile")]
public abstract class MunitionData : ScriptableObject
{
    public string munitionName;
    public GameObject munitionPrefab;
    public float damage;
    public float speed;
    public float selfDestructTime;
    public bool isHoming;
    public abstract void InitializeMunition(Munition munition);

}
