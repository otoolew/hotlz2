﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProjectileWeaponComponent : WeaponComponent
{
    [SerializeField] private MunitionPool munitionPool;
    public MunitionPool MunitionPool { get => munitionPool; set => munitionPool = value; }

    [SerializeField] private WeaponData weaponData;
    public WeaponData WeaponData { get => weaponData; set => weaponData = value; }

    [SerializeField] private float cooldownRate;
    public override float CooldownRate { get => cooldownRate; set => cooldownRate = value; }

    [SerializeField] private float cooldownTimer;
    public override float CooldownTimer { get => cooldownTimer; set => cooldownTimer = value; }

    [SerializeField] private bool weaponReady;
    public override bool WeaponReady { get => weaponReady; set => weaponReady = value; }

    [SerializeField] private Transform firePoint;
    public Transform FirePoint { get => firePoint; set => firePoint = value; }

    [SerializeField] private LayerMask targetMask;
    public LayerMask TargetMask { get => targetMask; set => targetMask = value; }

    // Start is called before the first frame update
    void Start()
    {
        //MunitionPool = FindObjectOfType<MunitionPool>();
        InitComponent();
    }

    // Update is called once per frame
    void Update()
    {
        weaponData.CooldownWeapon(this);
    }

    public override void Fire()
    {
        if (WeaponReady)
        {
            CooldownTimer = CooldownRate;
            WeaponReady = false;
            GameObject rocket = munitionPool.FetchFromPool();

            if (rocket != null)
            {
                rocket.transform.parent = null;
                rocket.transform.position = Vector3.zero;
                rocket.GetComponent<Munition>().SelfDestructTimer.ResetTimer();
                rocket.transform.position = firePoint.position;
                rocket.transform.rotation = firePoint.rotation;
                rocket.SetActive(true);
            }
        }
    }

    public override void InitComponent()
    {
        weaponData.Initialize(this);
    }   
}
