﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Weapons/Raycast Weapon")]
public class RaycastWeaponData : WeaponData
{
    public float weaponDamage;
    public float weaponRange;
    public float lineEffectDuration;

    private RaycastWeaponComponent raycastWeaponComponent;

    public override void Initialize(WeaponComponent weaponComponent)
    {
        raycastWeaponComponent = weaponComponent as RaycastWeaponComponent;
        raycastWeaponComponent.WeaponDamage = weaponDamage;
        raycastWeaponComponent.WeaponRange = weaponRange;
        raycastWeaponComponent.CooldownRate = cooldownRate;
        raycastWeaponComponent.CooldownRate = cooldownRate;
        raycastWeaponComponent.LineEffectDuration = lineEffectDuration;
    }

    public override void CooldownWeapon(WeaponComponent weaponComponent)
    {
        weaponComponent.CooldownTimer -= Time.deltaTime;

        if (weaponComponent.CooldownTimer <= 0)
        {
            weaponComponent.CooldownTimer = 0;
            weaponComponent.WeaponReady = true;
        }
        else
        {
            weaponComponent.WeaponReady = false;
        }
    }

}
