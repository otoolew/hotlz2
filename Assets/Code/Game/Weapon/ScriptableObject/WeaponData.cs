﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponData : ScriptableObject
{
    public string weaponName;
    public AudioClip soundEffectClip;
    public float cooldownRate;
    public bool isHoming;

    public abstract void Initialize(WeaponComponent weaponComponent);
    public abstract void CooldownWeapon(WeaponComponent weaponComponent);
}
