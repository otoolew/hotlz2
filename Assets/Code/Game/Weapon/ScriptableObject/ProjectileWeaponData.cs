﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(menuName = "Weapons/Projectile Weapon")]
public class ProjectileWeaponData : WeaponData
{
    public override void Initialize(WeaponComponent weaponComponent)
    {
        weaponComponent.GetComponent<WeaponComponent>().CooldownRate = cooldownRate;
        weaponComponent.GetComponent<WeaponComponent>().CooldownTimer = cooldownRate;
    }

    public override void CooldownWeapon(WeaponComponent weaponComponent)
    {
        weaponComponent.CooldownTimer -= Time.deltaTime;

        if (weaponComponent.CooldownTimer <= 0)
        {
            weaponComponent.CooldownTimer = 0;
            weaponComponent.WeaponReady = true;
        }
        else
        {
            weaponComponent.WeaponReady = false;
        }
    }
}
