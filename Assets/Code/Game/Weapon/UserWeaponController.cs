﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UserWeaponController : MonoBehaviour
{
    [SerializeField] private WeaponComponent[] weaponComponents;
    public WeaponComponent[] WeaponComponents { get => weaponComponents; private set => weaponComponents = value; }

    [SerializeField] private WeaponComponent equippedWeapon;
    public WeaponComponent EquippedWeapon { get => equippedWeapon; set => equippedWeapon = value; }

    [SerializeField] private int weaponIndex;
    public int WeaponIndex { get => weaponIndex; set => weaponIndex = value; }

    public LayerMask layerMask;
    private void Start()
    {
        weaponComponents = GetComponentsInChildren<WeaponComponent>();
        if(weaponComponents.Length > 0)
            equippedWeapon = weaponComponents[0];

    }
    private void Update()
    {
        AimPoint();
        if (Input.GetMouseButton(0))
            equippedWeapon.Fire();

        ChangeWeaponInput(Input.GetAxis("Mouse ScrollWheel"));

    }

    private void AimPoint()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out RaycastHit rayHit, layerMask))
        {
            Vector3 hitPoint = rayHit.point;
            Vector3 targetDir = hitPoint - equippedWeapon.transform.position;
            Vector3 newDir = Vector3.RotateTowards(equippedWeapon.transform.forward, targetDir, 10f, 0.0f);
            equippedWeapon.transform.rotation = Quaternion.LookRotation(targetDir);
        }
    }
    private void ChangeWeaponInput(float mouseWheelInput)
    {
        if (mouseWheelInput > 0f)// scroll up
        {
            //Debug.Log(abilitySlot);
            weaponIndex++;
            if (weaponIndex >= weaponComponents.Length)
                weaponIndex = 0;

            ChangeWeapon(weaponIndex);
        }
        else if (mouseWheelInput < 0f) // scroll down
        {
            //Debug.Log(abilitySlot);
            weaponIndex--;
            if (weaponIndex < 0)
                weaponIndex = weaponComponents.Length - 1;
            ChangeWeapon(weaponIndex);
        }
    }
    private void ChangeWeapon(int weaponIndex)
    {
        equippedWeapon = weaponComponents[weaponIndex];
    }
}
