﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HomingWeaponComponent : WeaponComponent
{
    [SerializeField] private MunitionPool munitionPool;
    public MunitionPool MunitionPool { get => munitionPool; set => munitionPool = value; }

    [SerializeField] private WeaponData weaponData;
    public WeaponData WeaponData { get => weaponData; set => weaponData = value; }

    [SerializeField] private float cooldownRate;
    public override float CooldownRate { get => cooldownRate; set => cooldownRate = value; }

    [SerializeField] private float cooldownTimer;
    public override float CooldownTimer { get => cooldownTimer; set => cooldownTimer = value; }

    [SerializeField] private bool weaponReady;
    public override bool WeaponReady { get => weaponReady; set => weaponReady = value; }

    [SerializeField] private Transform firePoint;
    public Transform FirePoint { get => firePoint; set => firePoint = value; }

    [SerializeField] private LayerMask targetMask;
    public LayerMask TargetMask { get => targetMask; set => targetMask = value; }

    [SerializeField] private Transform lockPaintedTarget;
    public Transform LockPaintedTarget { get => lockPaintedTarget; set => lockPaintedTarget = value; }

    public override void InitComponent()
    {
        weaponData.Initialize(this);
    }
    // Start is called before the first frame update
    void Start()
    {
        //MunitionPool = FindObjectOfType<MunitionPool>();
        InitComponent();
    }

    // Update is called once per frame
    void Update()
    {
        weaponData.CooldownWeapon(this);
    }
    public Transform LockOnTarget()
    {
        Ray ray = new Ray
        {
            origin = firePoint.position,
            direction = firePoint.forward
        };

        if (Physics.Raycast(ray, out RaycastHit rayHit, targetMask))
        {
            Targetable target = rayHit.collider.GetComponentInParent<Targetable>();
            if (target != null)
            {
                Debug.Log("Target Acquired!");
                return target.TargetTransform;
            }
        }
        return null;
    }
    public override void Fire()
    {
        lockPaintedTarget = LockOnTarget();
        if (lockPaintedTarget == null)
            return;
        if (WeaponReady)
        {
            CooldownTimer = CooldownRate;
            WeaponReady = false;
 
            if (lockPaintedTarget != null)
            {
                GameObject rocket = munitionPool.FetchFromPool();
                rocket.GetComponent<HomingRocketMunition>().LockedTarget = lockPaintedTarget;
                rocket.transform.parent = null;
                rocket.transform.position = Vector3.zero;
                rocket.GetComponent<Munition>().SelfDestructTimer.ResetTimer();
                rocket.transform.position = firePoint.position;
                rocket.transform.rotation = firePoint.rotation;
                rocket.SetActive(true);
                //Debug.Log("Locked Target Value " + rocket.GetComponent<HomingRocketMunition>().LockedTarget.name);
            }
        }
    }
}
