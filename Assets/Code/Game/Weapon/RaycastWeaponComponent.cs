﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(LineRenderer))]
public class RaycastWeaponComponent : WeaponComponent
{

    [SerializeField] private WeaponData weaponData;
    public WeaponData WeaponData { get => weaponData; set => weaponData = value; }

    //[SerializeField] private float weaponDamage;
    //public float WeaponDamage { get => weaponDamage; set => weaponDamage = value; }

    //[SerializeField] private float weaponRange;
    //public float WeaponRange { get => weaponRange; set => weaponRange = value; }
    public float WeaponDamage { get; set; }
    public float WeaponRange { get; set; }

    [SerializeField] private float cooldownRate;
    public override float CooldownRate { get => cooldownRate; set => cooldownRate = value; }

    [SerializeField] private float cooldownTimer;
    public override float CooldownTimer { get => cooldownTimer; set => cooldownTimer = value; }

    [SerializeField] private bool weaponReady;
    public override bool WeaponReady { get => weaponReady; set => weaponReady = value; }

    [SerializeField] private LineRenderer lineRenderer;
    public LineRenderer LineRenderer { get => lineRenderer; set => lineRenderer = value; }

    [SerializeField] private float lineEffectDuration;
    public float LineEffectDuration { get => lineEffectDuration; set => lineEffectDuration = value; }

    [SerializeField] private Transform firePoint;
    public Transform FirePoint { get => firePoint; set => firePoint = value; }

    [SerializeField] private LayerMask layerMask;
    public LayerMask LayerMask { get => layerMask; set => layerMask = value; }

    // Start is called before the first frame update
    void Start()
    {
        InitComponent();
        lineRenderer = GetComponent<LineRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        weaponData.CooldownWeapon(this);
    }

    public override void Fire()
    {
        if (WeaponReady)
        {
            CooldownTimer = CooldownRate;
            StopCoroutine(FireFX());
            StartCoroutine(FireFX());
        }
    }

    public override void InitComponent()
    {
        weaponData.Initialize(this);
    }
    IEnumerator FireFX()
    {
        WeaponReady = false;
        lineRenderer.enabled = true;
        lineRenderer.SetPosition(0, firePoint.position);
        Ray ray = new Ray
        {
            origin = firePoint.position,
            direction = firePoint.forward,
        };

        if (Physics.Raycast(ray, out RaycastHit raycastHit, WeaponRange, layerMask))
        {
            Vector3 hitPoint = raycastHit.point;
            Vector3 targetDir = hitPoint - firePoint.position;

            lineRenderer.SetPosition(0, firePoint.position);
            DamageZone damageZoneHit = raycastHit.collider.GetComponent<DamageZone>();
            if (damageZoneHit != null)
            {
                damageZoneHit.ApplyDamage(WeaponDamage);
            }

            lineRenderer.SetPosition(1, hitPoint);
            //Debug.Log("Hit Success " + raycastHit.collider.gameObject.name);
        }
        else
        {
            lineRenderer.SetPosition(1, ray.origin + ray.direction * WeaponRange);
        }

        yield return new WaitForSeconds(lineEffectDuration);
        lineRenderer.enabled = false;
    }
}
