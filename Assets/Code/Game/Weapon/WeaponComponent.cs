﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class WeaponComponent : MonoBehaviour
{
    public abstract float CooldownTimer { get; set; }
    public abstract float CooldownRate { get; set; }
    public abstract bool WeaponReady { get; set; }
    public abstract void InitComponent();
    public abstract void Fire();
}
