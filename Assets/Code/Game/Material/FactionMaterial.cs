﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "newFactionMaterial", menuName = "Faction/Material Template")]
public class FactionMaterial : ScriptableObject
{
    public Material flagMaterial;
    public Material soldierMaterial;
    public Material headquartersMaterial;
    public Material machinegunBaseMaterial;
    public Material machinegunTurretMaterial;
    public Material antiAirBaseMaterial;
    public Material antiAirTurretMaterial;

    public void ChangeSoldierMaterial(GameObject soldier)
    {
        soldier.transform.Find("Model").transform.Find("Head").GetComponent<SkinnedMeshRenderer>().material = soldierMaterial;
        soldier.transform.Find("Model").transform.Find("Body").GetComponent<SkinnedMeshRenderer>().material = soldierMaterial;
    }
    public void ChangeMachineGunMaterial(GameObject towerTurret)
    {
        towerTurret.transform.Find("Model").transform.Find("Base").GetComponent<MeshRenderer>().material = machinegunBaseMaterial;
        towerTurret.transform.Find("Model").transform.Find("Turret").GetComponent<MeshRenderer>().material = machinegunTurretMaterial;
    }
    public void ChangeHeadquartersMaterial(GameObject headquearters)
    {
        headquearters.transform.Find("Model").transform.Find("Headquarters").GetComponent<MeshRenderer>().material = headquartersMaterial;
    }
}
