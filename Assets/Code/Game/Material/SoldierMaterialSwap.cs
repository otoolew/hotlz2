﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoldierMaterialSwap : MaterialSwap
{
    public override void Swap(GameObject soldier, Material mat)
    {
        soldier.transform.Find("Model").transform.Find("Head").GetComponent<SkinnedMeshRenderer>().material = mat;
        soldier.transform.Find("Model").transform.Find("Body").GetComponent<SkinnedMeshRenderer>().material = mat;
    }
}
