﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TerritoryManager : Singleton<TerritoryManager>
{
    [SerializeField] private Territory[] allTerritories;
    public Territory[] AllTerritories { get => allTerritories; set => allTerritories = value; }

    [SerializeField] private Garrison[] allGarrisons;
    public Garrison[] AllGarrisons { get => allGarrisons; set => allGarrisons = value; }

    private void Start()
    {
        allTerritories = FindObjectsOfType<Territory>();
        allGarrisons = FindObjectsOfType<Garrison>();
    }
    public Territory FindClosestTerritory(Navigator requester)
    {
        if (AllTerritories.Length <= 0)
            Debug.Log("No Territories?");

        Territory nearestTerritory = null;

        float distance = float.MaxValue;
        for (int i = 0; i < AllTerritories.Length; i++)
        {
            Territory territory = AllTerritories[i];
            if(territory != requester.CurrentTerritory)
            {
                float currentDistance = Vector3.Distance(requester.transform.position, territory.transform.position);
                if (currentDistance < distance)
                {
                    distance = currentDistance;
                    nearestTerritory = territory;
                }
            }
        }

        return nearestTerritory;
    }
    public Garrison FindClosestGarrison(Transform requester)
    {
        if (AllGarrisons.Length <= 0)
            Debug.Log("No Garrisons?");

        Garrison nearestGarrison = null;

        float distance = float.MaxValue;
        for (int i = 0; i < AllGarrisons.Length; i++)
        {
            Garrison garrison = AllGarrisons[i];
            float currentDistance = Vector3.Distance(requester.position, garrison.transform.position);
            if (currentDistance < distance)
            {
                distance = currentDistance;
                nearestGarrison = garrison;
            }
        }

        return nearestGarrison;
    }
}
