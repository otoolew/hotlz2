﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Territory : MonoBehaviour
{
    [SerializeField] private FactionData factionData;
    public FactionData FactionData { get => factionData; set => factionData = value; }

    //[SerializeField] private TerritoryTower territoryTower;
    //public TerritoryTower TerritoryTower { get => territoryTower; set => territoryTower = value; }

    [SerializeField] private Objective objective;
    public Objective Objective { get => objective; set => objective = value; }

    [SerializeField] private Garrison[] garrisonPositions;
    public Garrison[] GarrisonPositions { get => garrisonPositions; private set => garrisonPositions = value; }

    [SerializeField] private List<Garrison> availableGarrionList;
    public List<Garrison> AvailableGarrionList { get => availableGarrionList; set => availableGarrionList = value; }

    public int totalDefensePositions;
    public int blueTroopCount;
    public int redTroopCount;

    // Start is called before the first frame update
    void Start()
    {
        LoadDefensePositions();
        UpdateTowerOwnership();
    }

    public void LoadDefensePositions()
    {
        garrisonPositions = GetComponentsInChildren<Garrison>();
        for (int i = 0; i < garrisonPositions.Length; i++)
        {
            garrisonPositions[i].GarrisonStateChanged.AddListener(HandleGarrisonStateChanged);
        }
        totalDefensePositions = garrisonPositions.Length;
    }

    public Garrison FindOpenGarrisonPosition(SoldierNavigation soldierNav)
    {
        if (availableGarrionList.Count <= 0)
            return null;
        Garrison nearestGarrison = null;
        float distance = float.MaxValue;
        for (int i = availableGarrionList.Count - 1; i >= 0; i--)
        {
            Garrison garrison = availableGarrionList[i];
            if (garrison.CurrentGarrisonState == Garrison.GarrisonState.OPEN)
            {
                float currentDistance = Vector3.Distance(transform.position, soldierNav.transform.position);
                if (currentDistance < distance)
                {
                    distance = currentDistance;
                    nearestGarrison = garrison;
                }
            }
        }
        return nearestGarrison;
    }
    public void UpdateTowerOwnership()
    {
        if (blueTroopCount > redTroopCount)
        {
            if (factionData != GameManager.Instance.FactionServer.Blue)
            {
                ChangeFactionOwnership(GameManager.Instance.FactionServer.Blue);
                //territoryTower.ChangeFactionOwnership(GameManager.Instance.FactionServer.BlueFactionManager.FactionData);
            }

        }
        else if (blueTroopCount < redTroopCount)
        {
            if (factionData != GameManager.Instance.FactionServer.Red)
            {
                ChangeFactionOwnership(GameManager.Instance.FactionServer.Red);
                //territoryTower.ChangeFactionOwnership(GameManager.Instance.FactionServer.RedFactionManager.FactionData);
            }
        }
        else
        {
            if (factionData != GameManager.Instance.FactionServer.Neutral)
            {
                ChangeFactionOwnership(GameManager.Instance.FactionServer.Neutral);
                //territoryTower.ChangeFactionOwnership(GameManager.Instance.FactionServer.NeutralFactionManager.FactionData);
            }
        }
    }

    public void ChangeFactionOwnership(FactionData newFaction)
    {
        factionData = newFaction;
    }

    private void HandleGarrisonStateChanged(Garrison garrison)
    {
        switch (garrison.CurrentGarrisonState)
        {
            case Garrison.GarrisonState.OPEN:
                AvailableGarrionList.Add(garrison);
                break;
            case Garrison.GarrisonState.CLOSED:
                if (AvailableGarrionList.Contains(garrison))
                    AvailableGarrionList.Remove(garrison);
                break;
            default:
                break;
        }
    }
}
