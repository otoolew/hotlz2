﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace GameEvent
{
    [Serializable] public class FadeComplete : UnityEvent<bool> { }
    [Serializable] public class SceneChangeComplete : UnityEvent<bool> { }

    [Serializable] public class FactionChange : UnityEvent<FactionData> { }
    [Serializable] public class GarrisonOpen : UnityEvent { }
    [Serializable] public class GarrisonClose : UnityEvent<Garrison> { }
    [Serializable] public class Captured : UnityEvent<CaptureComponent> { }

    [Serializable] public class DieEvent : UnityEvent { }

    [Serializable] public class RemoveUnitEvent : UnityEvent<UnitController> { }


}

