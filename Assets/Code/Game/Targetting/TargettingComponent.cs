﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SphereCollider))]
public class TargettingComponent : MonoBehaviour
{
    [SerializeField] private FactionData factionData;
    public FactionData FactionData { get => factionData; set => factionData = value; }

    [SerializeField] private float viewRadius;
    public float ViewRadius { get => viewRadius; set => viewRadius = value; }

    public LayerMask targetMask;
    public LayerMask obstacleMask;

    [SerializeField] private Targetable currentTarget;
    public Targetable CurrentTarget { get => currentTarget; set => currentTarget = value; }

    [SerializeField] private float searchRate;
    public float SearchRate { get => searchRate; set => searchRate = value; }

    [SerializeField] private float searchTimer;
    public float SearchTimer { get => searchTimer; set => searchTimer = value; }

    [SerializeField] private bool searchReady;
    public bool SearchReady { get => searchReady; set => searchReady = value; }

    [SerializeField] private bool hadTarget;
    public bool HadTarget { get => hadTarget; set => hadTarget = value; }

    public List<Targetable> TargetList = new List<Targetable>();

    public event Action<Targetable> TargetAcquired;
    public event Action TargetLost;

    private void OnEnable()
    {
        ResetTargetter();
    }
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<SphereCollider>().radius = viewRadius;
        GetComponent<SphereCollider>().isTrigger = true;
    }

    // Update is called once per frame
    void Update()
    {
        CooldownSearch();
        if (searchReady)
        {
            CurrentTarget = GetNearestTarget();
            if (CurrentTarget != null)
                TargetAcquired?.Invoke(CurrentTarget);
            searchTimer = searchRate;
            searchReady = false;
        }

        hadTarget = CurrentTarget != null;
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.isTrigger)
            return;
        Targetable targetUnit = other.transform.root.GetComponent<Targetable>();
        if (targetUnit == null)
            return;
        if (IsTargetValid(targetUnit))
        {
            if (TargetVisable(targetUnit))
            {
                targetUnit.Removed += OnTargetRemoved;
                TargetList.Add(targetUnit);
                TargetAcquired?.Invoke(targetUnit);
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        //UnitActor targetUnit = other.transform.root.GetComponent<UnitActor>();
        Targetable targetUnit = other.GetComponentInParent<Targetable>();
        if (targetUnit == null)
        {
            targetUnit = other.transform.root.GetComponent<Targetable>();
            if (targetUnit == null)
            {
                return;
            }
        }

        if (!IsTargetValid(targetUnit))
            return;
        TargetList.Remove(targetUnit);

        if (targetUnit == CurrentTarget)
        {
            OnTargetRemoved(targetUnit);
        }
        else
        {
            // Only need to remove if we're not our actual target, otherwise OnTargetRemoved will do the work above
            targetUnit.Removed -= OnTargetRemoved;
        }
    }
    private void OnTargetRemoved(Damageable targetUnit)
    {
        targetUnit.Removed -= OnTargetRemoved;
        if (CurrentTarget != null && CurrentTarget == targetUnit)
        {
            TargetLost?.Invoke();
            CurrentTarget = null;
        }
        else
        {
            for (int i = 0; i < TargetList.Count; i++)
            {
                if (TargetList[i].GetComponent<Damageable>() == targetUnit)
                {
                    TargetList.RemoveAt(i);
                    break;
                }
            }
        }
    }
    public bool IsTargetValid(Targetable targettedUnit)
    {
        if (targettedUnit.FactionData == null)
            return false;
        return factionData.CanHarm(targettedUnit.FactionData);
    }

    public bool TargetVisable(Targetable targetUnit)
    {
        if ((targetUnit != null) && (targetUnit.gameObject.activeInHierarchy))
        {
            Vector3 directionToTargetable = (targetUnit.TargetTransform.position - transform.position);
            float distanceToTargetable = Vector3.Distance(transform.position, targetUnit.TargetTransform.position);

            if(!Physics.Raycast(transform.position, directionToTargetable, distanceToTargetable, obstacleMask))
            {
                Debug.DrawRay(transform.position, directionToTargetable, Color.red, 0.2f, false);
                //No Obsticles in way
                return true;
            }
            
        }
        return false;
    }
    public bool TargetInRange(Targetable targetUnit)
    {
        if ((targetUnit != null))
        {
            float distanceToTargetable = Vector3.Distance(transform.position, targetUnit.transform.position);
            if (distanceToTargetable <= (viewRadius * 2))
                return true;
        }
        return false;
    }

    public Targetable GetNearestTarget()
    {
        int length = TargetList.Count;
        if (length == 0)
            return null;
        if (length == 0)
            return null;
        Targetable nearestTarget = null;
        float distance = float.MaxValue;
        for (int i = TargetList.Count - 1; i >= 0; i--)
        {
            Targetable targetUnit = TargetList[i];

            if (targetUnit == null || !TargetInRange(targetUnit))
            {
                TargetList.RemoveAt(i);
                OnTargetRemoved(targetUnit);
            }
            float currentDistance = Vector3.Distance(transform.position, targetUnit.transform.position);
            if (currentDistance < distance)
            {
                distance = currentDistance;
                nearestTarget = targetUnit;
            }
        }
        return nearestTarget;
    }
    public Vector3 DirectionFromAngle(float angleInDegrees, bool angleIsGlobal)
    {
        if (!angleIsGlobal)
        {
            angleInDegrees += transform.eulerAngles.y;
        }
        return new Vector3(Mathf.Sin(angleInDegrees * Mathf.Deg2Rad), 0, Mathf.Cos(angleInDegrees * Mathf.Deg2Rad));
    }
    public void ResetTargetter()
    {
        TargetList.Clear();
        CurrentTarget = null;
        TargetAcquired = null;
        TargetLost = null;
    }
    public void CooldownSearch()
    {
        searchTimer -= Time.deltaTime;
        if (searchTimer <= 0f)
        {
            searchReady = true;
            return;
        }
        searchReady = false;       
    }
}
