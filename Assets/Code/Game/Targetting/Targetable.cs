﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameEnum;

public class Targetable : Damageable
{
    [SerializeField] private TargetType targetType;
    public TargetType TargetType { get => targetType; set => targetType = value; }

    [SerializeField] private FactionData factionData;
    public FactionData FactionData { get => factionData; set => factionData = value; }

    [SerializeField] private Transform targetTransform;
    public Transform TargetTransform { get { return targetTransform ?? transform;  } set { targetTransform = value; } }

    /// <summary>
    /// Initializes any Damageable logic
    /// </summary>
    protected override void Awake()
    {
        base.Awake();
    }

}
