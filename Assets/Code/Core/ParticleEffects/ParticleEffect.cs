﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleEffect : MonoBehaviour
{
    public ParticleSystem particleEffect;
    // Start is called before the first frame update
    void Start()
    {
        if (particleEffect == null)
            particleEffect.GetComponentInChildren<ParticleEffect>();
    }
    public void Activate()
    {
        particleEffect.gameObject.SetActive(true);
    }
    public void Deactivate()
    {
        particleEffect.gameObject.SetActive(false);
    }
}
