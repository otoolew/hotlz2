﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIManager : MonoBehaviour
{
    [SerializeField] private PauseMenu pauseMenu;
    [SerializeField] private GameOverMenu gameOverMenu;

    private void Start()
    {
        // Subscribe / Listen to the GameManager EventGameState OnGameStateChanged event  
        GameManager.Instance.OnGameStateChanged.AddListener(HandleGameStateChanged);
    }
    /// <summary>
    /// Handles what happens when the UIManager is notified of a GameState change.
    /// Arguements taken in are the EventArgs Parameters
    /// </summary>
    /// <param name="currentState"></param>
    /// <param name="previousState"></param>
    private void HandleGameStateChanged(GameManager.GameState currentState, GameManager.GameState previousState)
    {
        if (currentState == GameManager.GameState.RUNNING)
        {
            pauseMenu.gameObject.SetActive(false);
            gameOverMenu.gameObject.SetActive(false);
            return;
        }

        if (currentState == GameManager.GameState.PAUSED)
        {
            pauseMenu.gameObject.SetActive(true);
            return;
        }
        if (currentState == GameManager.GameState.GAMEOVER)
        {
            gameOverMenu.gameObject.SetActive(true);
            return;
        }
    }
}
