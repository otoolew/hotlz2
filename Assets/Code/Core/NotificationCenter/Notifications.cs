﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Notifications
{
    public const string TEST_NOTIFICATION = "TEST_NOTIFICATION";
    public const string UNIT_REMOVE_NOTIFICATION = "UNIT_REMOVE_NOTIFICATION";
}
