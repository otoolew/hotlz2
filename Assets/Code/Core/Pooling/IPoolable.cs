﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPoolable
{
    bool Pooled { get; set; }
    void Repool();
}
