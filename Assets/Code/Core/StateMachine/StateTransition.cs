﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.StateMachine
{
    [Serializable]
    public class StateTransition
    {
        public StateDecision decision;
        public State decisionTrueState;
        public State decisionFalseState;

    }
}
