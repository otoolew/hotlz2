﻿using Core.StateMachine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.StateMachine
{

    public abstract class StateAction : ScriptableObject
    {
        public abstract void PerformAction(StateMachine stateMachine);
    }
}