﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.StateMachine
{
    public abstract class StateDecision : ScriptableObject
    {
        public abstract bool Decide(StateMachine stateMachine);
    }
}