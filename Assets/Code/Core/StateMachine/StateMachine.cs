﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.StateMachine
{
    public class StateMachine : MonoBehaviour
    {
        public State CurrentState;
        public float currentStateUpdateFrequency;
        public float timer;
        protected virtual void Update()
        {
            UpdateTimer();
        }
        protected virtual void ChangeState(State freshState)
        {
            CurrentState.OnStateExit(this);
            CurrentState = freshState;
            currentStateUpdateFrequency = freshState.updateFrequency;
            CurrentState.OnStateEnter(this);
        } 
        public void UpdateTimer()
        {
            timer -= Time.deltaTime;
            if(timer <= 0)
            {
                CurrentState.OnStateUpdate(this);
                timer = currentStateUpdateFrequency;
            }
        }
    }
}