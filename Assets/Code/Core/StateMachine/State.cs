﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace Core.StateMachine
{

    public abstract class State : ScriptableObject
    {
        public float updateFrequency;

        public abstract void OnStateEnter(StateMachine stateMachine);

        public abstract void OnStateUpdate(StateMachine stateMachine);

        public abstract void OnStateExit(StateMachine stateMachine);

    }
}