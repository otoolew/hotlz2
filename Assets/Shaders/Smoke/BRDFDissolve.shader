Shader "Custom/BRDF Dissolve" {
Properties {
	_Color ("Tint Color", Color) = (1,1,1,1)
	_MainTex ("Dissolve Texture", 2D) = "white" {}
	_BRDFTex ("BRDF (RGB)", 2D) = "black" {}
	_LightIntensity ("Light Intensity", Range(0.0,1.0)) = 1.0
	_PanU ("U Pan", Float) = 0.0
	_PanV ("V Pan", Float) = 0.0
}

Category {
	Tags {"RenderType"="TransparentCutout"}
	Cull Off 

	SubShader {

		CGPROGRAM
		// Physically based Standard lighting model, and enable shadows on all light types
		#pragma surface surf Ramp addshadow
		// Use shader model 3.0 target, to get nicer looking lighting
		#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _BRDFTex;
			
		half _PanU;
		half _PanV;
		half _LightIntensity;

		struct Input {
			float2 uv_MainTex;
			float2 uv_BRDFTex;
			float4 color:COLOR;
		};

		half4 LightingRamp (SurfaceOutput s, half3 lightDir, half3 viewDir, half atten)
		{ 
			float NdotL = dot(s.Normal, lightDir);
			float NdotE = dot(s.Normal, viewDir);

			//diffuse wrap
			float diff = (NdotL * 0.5) + 0.5;
			float2 brdfUV = float2(NdotE, diff);
			float3 BRDF = tex2D(_BRDFTex, brdfUV.xy);
			float4 c;
			c.rgb = (s.Albedo * (1-_LightIntensity)) + (s.Albedo * (_LightColor0.rgb * BRDF * atten * _LightIntensity));
			c.a = s.Alpha; 

			return c;
		}

		fixed4 _Color;
		void surf (Input IN, inout SurfaceOutput o) {

			float2 panUV = IN.uv_MainTex;
			panUV.x += _Time.r * _PanU;
			panUV.y += _Time.r * _PanV;
				
			fixed4 distort = (tex2D (_MainTex, panUV) - 0.5);
			fixed4 col = IN.color * _Color;
			clip(col.a + distort - 0.5);

			o.Albedo = IN.color.rgb * _Color.rgb;
			o.Alpha = 1;
		}
		ENDCG
	}	
}
}
